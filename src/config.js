const apiUrl = "https://static.ui.com/fingerprint/ui/public.json";
const locale = "en";

const config = { apiUrl, locale };

export default config;
