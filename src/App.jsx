import config from "./config";
import { IntlProvider } from "react-intl";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import messages from "./translations/en.json";
import { Device, SearchResults } from "./views";

function App() {
  return (
    <IntlProvider locale={config.locale} messages={messages} textComponent="span">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<SearchResults />} />
          <Route path="/device/:id" element={<Device />} />
        </Routes>
      </BrowserRouter>
    </IntlProvider>
  );
}

export default App;
