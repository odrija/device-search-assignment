import React, { useState } from "react";
import { array, func, oneOf, shape } from "prop-types";
import { injectIntl, FormattedMessage } from "react-intl";
import { FilterPanel, IconClear, IconGrid, IconList, IconSearch } from "..";

import css from "./Toolbar.module.css";

const Toolbar = (props) => {
  const {
    handleSelectFilterCheckbox,
    handleSearch,
    handleResetSearch,
    handleSelectGrid,
    handleSelectList,
    selectedLines,
    view,
    intl,
  } = props;
  const [filtersOpen, setFiltersOpen] = useState(false);
  const [keyword, setKeyword] = useState(null);
  const searchPlaceholder = intl.formatMessage({
    id: "Toolbar.searchPlaceholder",
  });

  return (
    <nav className={css.root}>
      <div className={css.container}>
        <form onSubmit={(e) => handleSearch(e, keyword)}>
          <div className={css.searchField}>
            <IconSearch />
            <input
              name="keyword"
              placeholder={searchPlaceholder}
              onChange={(e) => setKeyword(e.target.value)}
            />
            <button
              className={css.searchClear}
              type="reset"
              onClick={handleResetSearch}
            >
              <IconClear />
            </button>
          </div>

          <div className={css.buttons}>
            <button
              className={css.viewToggle}
              type="button"
              onClick={handleSelectList}
            >
              <IconList state={view === "list" ? "active" : "normal"} />
            </button>
            <button
              className={css.viewToggle}
              type="button"
              onClick={handleSelectGrid}
            >
              <IconGrid state={view === "grid" ? "active" : "normal"} />
            </button>
            <button
              className={css.filterToggle}
              type="button"
              onClick={() => setFiltersOpen(true)}
            >
              <FormattedMessage id="Toolbar.filter" />
            </button>
          </div>

          <FilterPanel
            visible={filtersOpen}
            handleSelectFilterCheckbox={handleSelectFilterCheckbox}
            handleClose={() => setFiltersOpen(false)}
            selectedLines={selectedLines}
          />
        </form>
      </div>
    </nav>
  );
};

Toolbar.defaultProps = { selectedLines: [], view: "list" };

Toolbar.propTypes = {
  handleSelectFilterCheckbox: func.isRequired,
  handleSearch: func.isRequired,
  handleResetSearch: func.isRequired,
  handleSelectGrid: func.isRequired,
  handleSelectList: func.isRequired,
  selectedLines: array,
  view: oneOf(["list", "grid"]),
  intl: shape({
    formatMessage: func.isRequired,
  }),
};

export default injectIntl(Toolbar);
