import React from "react";
import { array, func, number } from "prop-types";
import { useNavigate } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { Pagination } from "..";

import css from "./DeviceList.module.css";

const DeviceList = ({ devices, paginate, currentPage, handlePaginate }) => {
  const page = paginate(devices, currentPage);
  const navigate = useNavigate();

  return (
    <div className={css.root}>
      <div className={css.container}>
        <table className={css.table}>
          <thead>
            <tr>
              <th>
                <FormattedMessage
                  id="DeviceList.count"
                  values={{ count: devices.length }}
                />
              </th>
              <th>
                <FormattedMessage id="DeviceList.productTitle" />
              </th>
              <th>
                <FormattedMessage id="DeviceList.name" />
              </th>
            </tr>
          </thead>
          <tbody>
            {page &&
              page.data.map((item) => (
                <tr
                  key={item.shortnames[0]}
                  onClick={() =>
                    navigate(`/device/${item.shortnames[0].toLowerCase()}`)
                  }
                >
                  <td>
                    <img
                      src={`https://static.ui.com/fingerprint/ui/icons/${item.icon.id}_25x25.png`}
                    />
                  </td>
                  <td>{item.line.name}</td>
                  <td>{item.product.name}</td>
                </tr>
              ))}
          </tbody>
        </table>
        <Pagination handlePaginate={handlePaginate} paginateObj={page} />
      </div>
    </div>
  );
};

DeviceList.defaultProps = { currentPage: 1 };

DeviceList.propTypes = {
  devices: array.isRequired,
  paginate: func.isRequired,
  currentPage: number,
  handlePaginate: func.isRequired,
};

export default DeviceList;
