import React from "react";
import { oneOf } from "prop-types";

const Logo = ({ state }) => {
  return state === "hover" ? (
    <svg width="55" height="55" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path transform="translate(.5)" fill="url(#a)" d="M0 0h56v56H0z" />
      <path transform="translate(.5)" fill="url(#b)" d="M0 0h56v56H0z" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M34.67 15.695c-.852 1.112-1.278 2.315-1.278 3.61v14.82a13.388 13.388 0 0 1-.222 2.379c.945-.182 1.81-.46 2.597-.834a8.424 8.424 0 0 0 2.125-1.436 8.924 8.924 0 0 0 1.64-2.064 13.12 13.12 0 0 0 1.166-2.693l.611-1.97v-13.48c-3.574 0-5.787.556-6.639 1.668Zm-12.305-.136H20.78v1.53h1.584v-1.53Zm1.583 2.324h-1.583v1.531h1.583v-1.531Zm-1.583 1.941H20.78v1.531h1.584v-1.53Zm-1.584 2.297h-1.583v1.559h1.583V22.12ZM17.615 14H16.03v1.559h1.584V14Zm7.069 23.598c-.49-.985-.736-2.142-.736-3.473V22.887h-1.583v5.742H20.78V25.21h-1.583v1.531h-1.583v-8.476H16.03v12.687c0 1.568.287 3.026.861 4.375a10.058 10.058 0 0 0 2.486 3.514c1.084.993 2.408 1.768 3.973 2.324 1.565.556 3.338.834 5.32.834l-.737-.478c-.49-.32-1.032-.807-1.625-1.463-.593-.657-1.134-1.477-1.625-2.461Zm9.125-.602c-1.222.273-2.565.383-4.028.328-1.055-.054-1.995-.205-2.82-.451a9.305 9.305 0 0 1-2.18-.943c.426 1.823 1.213 3.185 2.361 4.088 1.149.902 1.76 1.38 1.834 1.435l.944.492c1.796-.11 3.398-.465 4.806-1.066 1.407-.602 2.602-1.39 3.583-2.365a9.84 9.84 0 0 0 2.236-3.391c.51-1.285.764-2.675.764-4.17v-1.312c-.426 1.385-.99 2.575-1.694 3.568a9.177 9.177 0 0 1-2.514 2.434c-.972.629-2.07 1.08-3.292 1.353ZM20.754 17.117h-1.556v1.531h1.555v-1.53ZM42.163 14c.148 0 .277.053.388.159a.507.507 0 0 1 .167.38c0 .159-.056.29-.167.396a.545.545 0 0 1-.769 0 .537.537 0 0 1-.158-.396c0-.148.053-.275.158-.38a.519.519 0 0 1 .38-.159Zm0 1.014a.462.462 0 0 0 .46-.476.443.443 0 0 0-.135-.324.443.443 0 0 0-.325-.135.417.417 0 0 0-.317.135.457.457 0 0 0-.127.325c0 .137.042.25.127.34.084.09.19.136.317.136Zm.214-.483c.026-.026.04-.066.04-.119 0-.063-.016-.11-.048-.143-.032-.031-.09-.047-.174-.047h-.238v.634h.095v-.27h.11l.175.27h.095l-.174-.27a.228.228 0 0 0 .119-.055Zm-.214-.024a.35.35 0 0 0 .11-.016c.033-.01.048-.037.048-.079s-.013-.071-.04-.087a.198.198 0 0 0-.102-.024h-.127v.206h.11Z"
        fill="url(#c)"
      />
      <defs>
        <radialGradient
          id="a"
          cx="0"
          cy="0"
          r="1"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(0 56 -56 0 28 0)"
        >
          <stop stopColor="#006FFF" />
          <stop offset="1" stopColor="#003C9E" />
        </radialGradient>
        <radialGradient
          id="b"
          cx="0"
          cy="0"
          r="1"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(0 56 -56 0 28 0)"
        >
          <stop stopColor="#005ED9" />
          <stop offset="1" stopColor="#003386" />
        </radialGradient>
        <linearGradient
          id="c"
          x1="24.847"
          y1="57.046"
          x2="44.053"
          y2="33.452"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C2C4CE" />
          <stop offset=".397" stopColor="#D6D9E2" />
          <stop offset="1" stopColor="#fff" />
        </linearGradient>
      </defs>
    </svg>
  ) : (
    <svg width="55" height="55" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path transform="translate(.5)" fill="url(#a)" d="M0 0h56v56H0z" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M34.67 15.695c-.852 1.112-1.278 2.315-1.278 3.61v14.82a13.388 13.388 0 0 1-.222 2.379c.945-.182 1.81-.46 2.597-.834a8.424 8.424 0 0 0 2.125-1.436 8.924 8.924 0 0 0 1.64-2.064 13.12 13.12 0 0 0 1.166-2.693l.611-1.97v-13.48c-3.574 0-5.787.556-6.639 1.668Zm-12.305-.136H20.78v1.53h1.584v-1.53Zm1.583 2.324h-1.583v1.531h1.583v-1.531Zm-1.583 1.941H20.78v1.531h1.584v-1.53Zm-1.584 2.297h-1.583v1.559h1.583V22.12ZM17.615 14H16.03v1.559h1.584V14Zm7.069 23.598c-.49-.985-.736-2.142-.736-3.473V22.887h-1.583v5.742H20.78V25.21h-1.583v1.531h-1.583v-8.476H16.03v12.687c0 1.568.287 3.026.861 4.375a10.058 10.058 0 0 0 2.486 3.514c1.084.993 2.408 1.768 3.973 2.324 1.565.556 3.338.834 5.32.834l-.737-.478c-.49-.32-1.032-.807-1.625-1.463-.593-.657-1.134-1.477-1.625-2.461Zm9.125-.602c-1.222.273-2.565.383-4.028.328-1.055-.054-1.995-.205-2.82-.451a9.305 9.305 0 0 1-2.18-.943c.426 1.823 1.213 3.185 2.361 4.088 1.149.902 1.76 1.38 1.834 1.435l.944.492c1.796-.11 3.398-.465 4.806-1.066 1.407-.602 2.602-1.39 3.583-2.365a9.84 9.84 0 0 0 2.236-3.391c.51-1.285.764-2.675.764-4.17v-1.312c-.426 1.385-.99 2.575-1.694 3.568a9.177 9.177 0 0 1-2.514 2.434c-.972.629-2.07 1.08-3.292 1.353ZM20.754 17.117h-1.556v1.531h1.555v-1.53ZM42.163 14c.148 0 .277.053.388.159a.507.507 0 0 1 .167.38c0 .159-.056.29-.167.396a.545.545 0 0 1-.769 0 .537.537 0 0 1-.158-.396c0-.148.053-.275.158-.38a.519.519 0 0 1 .38-.159Zm0 1.014a.462.462 0 0 0 .46-.476.443.443 0 0 0-.135-.324.443.443 0 0 0-.325-.135.417.417 0 0 0-.317.135.457.457 0 0 0-.127.325c0 .137.042.25.127.34.084.09.19.136.317.136Zm.214-.483c.026-.026.04-.066.04-.119 0-.063-.016-.11-.048-.143-.032-.031-.09-.047-.174-.047h-.238v.634h.095v-.27h.11l.175.27h.095l-.174-.27a.228.228 0 0 0 .119-.055Zm-.214-.024a.35.35 0 0 0 .11-.016c.033-.01.048-.037.048-.079s-.013-.071-.04-.087a.198.198 0 0 0-.102-.024h-.127v.206h.11Z"
        fill="url(#b)"
      />
      <defs>
        <radialGradient
          id="a"
          cx="0"
          cy="0"
          r="1"
          gradientUnits="userSpaceOnUse"
          gradientTransform="matrix(0 56 -56 0 28 0)"
        >
          <stop stopColor="#006FFF" />
          <stop offset="1" stopColor="#003C9E" />
        </radialGradient>
        <linearGradient
          id="b"
          x1="24.847"
          y1="57.046"
          x2="44.053"
          y2="33.452"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#C2C4CE" />
          <stop offset=".397" stopColor="#D6D9E2" />
          <stop offset="1" stopColor="#fff" />
        </linearGradient>
      </defs>
    </svg>
  );
};

Logo.defaultProps = { state: "normal" };

Logo.propTypes = { state: oneOf(["normal", "hover"]) };

export default Logo;
