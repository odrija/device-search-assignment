import React from "react";
import { array, func, number } from "prop-types";
import { FormattedMessage } from "react-intl";
import { DeviceCard, Pagination } from "..";

import css from "./DeviceGrid.module.css";

const DeviceGrid = ({ devices, paginate, currentPage, handlePaginate }) => {
  const page = paginate(devices, currentPage);

  return (
    <div className={css.root}>
      <div className={css.container}>
        <div className={css.count}>
          <FormattedMessage
            id="DeviceGrid.count"
            values={{ count: devices.length }}
          />
        </div>
        <div className={css.grid}>
          {page &&
            page.data.map((item) => (
              <DeviceCard device={item} key={item.shortnames[0]} />
            ))}
        </div>
        <Pagination handlePaginate={handlePaginate} paginateObj={page} />
      </div>
    </div>
  );
};

DeviceGrid.defaultProps = { currentPage: 1 };

DeviceGrid.propTypes = {
  devices: array.isRequired,
  paginate: func.isRequired,
  currentPage: number,
  handlePaginate: func.isRequired,
};

export default DeviceGrid;
