import React, { useState } from "react";
import { FormattedMessage } from "react-intl";
import { Logo } from "..";

import css from "./Header.module.css";

const Header = () => {
  const [logoHovered, setLogoHovered] = useState(false);

  return (
    <header className={css.root}>
      <a
        className={css.logo}
        href="https://ui.com"
        title="Ubiquiti Homepage"
        onMouseEnter={() => setLogoHovered(true)}
        onMouseLeave={() => setLogoHovered(false)}
      >
        <Logo state={logoHovered ? "hover" : "normal"} />
      </a>

      <h1 className={css.title}>
        <FormattedMessage id="Header.title" />
      </h1>

      <span className={css.author}>
        <FormattedMessage id="Header.author" />
      </span>
    </header>
  );
};

export default Header;
