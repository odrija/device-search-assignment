import React from "react";
import { array, bool, func } from "prop-types";
import { FormattedMessage } from "react-intl";
import { IconClear } from "..";
import classNames from "classnames";

import css from "./FilterPanel.module.css";

const FilterPanel = (props) => {
  const { handleClose, handleSelectFilterCheckbox, selectedLines, visible } = props;
  const productLines = [
    "UniFi",
    "UniFi LTE",
    "UniFi Protect",
    "UniFi Access",
    "airMAX",
    "EdgeMAX",
  ];

  return (
    <nav
      className={classNames(css.root, {
        [css.visible]: visible,
      })}
    >
      <header className={css.header}>
        <FormattedMessage id="FilterPanel.title" />
        <button className={css.close} type="button" onClick={handleClose}>
          <IconClear />
        </button>
      </header>
      <div className={css.body}>
        <p className={css.filtersTitle}>
          <FormattedMessage id="FilterPanel.filtersTitle" />
        </p>
        {productLines.map((item) => (
          <div className={css.filtersItem} key={item}>
            <label>
              <input
                type="checkbox"
                name="productLine[]"
                value={item}
                onChange={handleSelectFilterCheckbox}
                checked={selectedLines.includes(item)}
              />
              <span className={css.styledCheckbox}></span>
              {item}
            </label>
          </div>
        ))}
      </div>
    </nav>
  );
};

FilterPanel.defaultProps = { selectedLines: [], visible: false };

FilterPanel.propTypes = {
  handleClose: func.isRequired,
  handleSelectFilterCheckbox: func.isRequired,
  selectedLines: array,
  visible: bool,
};

export default FilterPanel;
