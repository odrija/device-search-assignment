import React from "react";
import { object } from "prop-types";
import { Link } from "react-router-dom";

import css from "./DeviceCard.module.css";

const DeviceCard = ({ device }) => {
  const deviceLink = `/device/${device.shortnames[0].toLowerCase()}`;
  return (
    <article className={css.root}>
      <Link to={deviceLink}>
        <div className={css.image}>
          <img
            src={`https://static.ui.com/fingerprint/ui/icons/${device.icon.id}_257x257.png`}
          />
        </div>
        <div className={css.info}>
          <div className={css.productName}>{device.product.name}</div>
          <div className={css.lineName}>{device.line.name}</div>
        </div>
      </Link>
    </article>
  );
};

DeviceCard.propTypes = { device: object.isRequired };

export default DeviceCard;
