import React from "react";
import classNames from "classnames";
import { object } from "prop-types";

import css from "./Pagination.module.css";

const Pagination = ({ handlePaginate, paginateObj }) => {
  const PageNav = () => {
    let nav = [];
    for (let i = 1; i <= paginateObj.totalPages; i++) {
      nav.push(
        <button
          className={classNames(css.button, {
            [css.buttonActive]: i === paginateObj.page,
          })}
          key={i}
          type="button"
          onClick={() => handlePaginate(i)}
        >
          {i}
        </button>
      );
    }
    return nav.length > 1 ? <>{nav}</> : null;
  };

  return (
    <nav className={css.root}>
      <PageNav />
    </nav>
  );
};

Pagination.propTypes = { paginateObj: object.isRequired };

export default Pagination;
