import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import config from "../../config";
import { DeviceGrid, DeviceList, Header, Toolbar } from "../../components";

import css from "./SearchResults.module.css";

import spinner from "../../assets/spinner.gif";
import errorImg from "../../assets/error.svg";

const devicesPerPage = 50;

const paginate = (items, currentPage) => {
  const page = currentPage || 1,
    offset = (page - 1) * devicesPerPage,
    paginatedItems = items.slice(offset).slice(0, devicesPerPage),
    totalPages = Math.ceil(items.length / devicesPerPage);

  return {
    page: page,
    prevPage: page - 1 ? page - 1 : null,
    nextPage: totalPages > page ? page + 1 : null,
    total: items.length,
    totalPages,
    data: paginatedItems,
  };
};

const SearchResults = () => {
  const [allDevices, setAllDevices] = useState(null);
  const [searchResults, setSearchResults] = useState(null);
  const [filteredResults, setFilteredResults] = useState(null);
  const [error, setError] = useState(null);
  const [view, setView] = useState("list");
  const [selectedLines, setSelectedLines] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [searchParams] = useSearchParams();
  const initialView = searchParams.get("view");
  const navigate = useNavigate();

  const search = (e, keyword) => {
    e.preventDefault();
    const query = keyword.toLowerCase();
    let result = [];
    allDevices.forEach((item) => {
      if (
        item.line.name.toLowerCase().indexOf(query) !== -1 ||
        item.product.name.toLowerCase().indexOf(query) !== -1
      ) {
        result.push(item);
      }
    });
    setSearchResults(result);
    setFilteredResults(null);
    setCurrentPage(1);
  };

  const handleSelectFilterCheckbox = (e) => {
    if (!e.target.checked) {
      const indexToRemove = selectedLines.indexOf(e.target.value);
      if (indexToRemove > -1) {
        const newSelectedLines = [...selectedLines];
        newSelectedLines.splice(indexToRemove, 1);
        setSelectedLines(newSelectedLines);
      }
    } else {
      const newSelectedLines = [...selectedLines];
      newSelectedLines.push(e.target.value);
      setSelectedLines(newSelectedLines);
    }
  };

  const handleSwitchView = (view) => {
    navigate({
      pathname: "/",
      search: `?view=${view}`,
    });
    setView(view);
  };
  
  const handleResetSearch = () => {
    setSearchResults(null);
    setFilteredResults(null);
    setSelectedLines([]);
  };

  useEffect(() => {
    if (initialView && (initialView === "list" || initialView === "grid")) {
      setView(initialView);
    }

    axios
      .get(config.apiUrl)
      .then((response) => {
        if (response.data.devices) {
          setAllDevices(response.data.devices);
        }
      })
      .catch((error) => {
        setError(error);
      });
  }, []);

  useEffect(() => {
    const devices = searchResults || allDevices;
    if (selectedLines.length > 0) {
      let result = [];
      devices.filter((item) => {
        if (selectedLines.indexOf(item.line.name) > -1) {
          result.push(item);
        }
      });
      setFilteredResults(result);
    } else {
      setFilteredResults(devices);
    }
    setCurrentPage(1);
  }, [selectedLines]);

  return (
    <div className={css.root}>
      <Header />
      <Toolbar
        handleSearch={search}
        handleSelectFilterCheckbox={handleSelectFilterCheckbox}
        handleResetSearch={handleResetSearch}
        handleSelectGrid={() => handleSwitchView("grid")}
        handleSelectList={() => handleSwitchView("list")}
        selectedLines={selectedLines}
        view={view}
      />
      {error ? (
        <div className={css.errorWrapper}>
          <img src={errorImg} />
          <h1>
            <FormattedMessage id="SearchResults.error" />
          </h1>
        </div>
      ) : allDevices ? (
        view === "grid" ? (
          <DeviceGrid
            devices={filteredResults || searchResults || allDevices}
            paginate={paginate}
            currentPage={currentPage}
            handlePaginate={setCurrentPage}
          />
        ) : (
          <DeviceList
            devices={filteredResults || searchResults || allDevices}
            paginate={paginate}
            currentPage={currentPage}
            handlePaginate={setCurrentPage}
          />
        )
      ) : (
        <div className={css.spinnerWrapper}>
          <img src={spinner} alt="Loading…" height="64" width="64" />
        </div>
      )}
    </div>
  );
};

export default SearchResults;
