import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import config from "../../config";
import { Header, IconBack } from "../../components";

import css from "./Device.module.css";

import spinner from "../../assets/spinner.gif";
import errorImg from "../../assets/error.svg";

const Device = () => {
  const navigate = useNavigate();
  const params = useParams();
  const [searchResults, setSearchResults] = useState(null);
  const [device, setDevice] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get(config.apiUrl)
      .then((response) => {
        setSearchResults(response.data);
      })
      .catch((error) => {
        setError(error);
      });
  }, []);

  useEffect(() => {
    if (searchResults?.devices) {
      const shortname = params.id.toUpperCase();
      const deviceFound = searchResults.devices.find((item) => {
        const found = item.shortnames.filter((str) =>
          str.toUpperCase().includes(shortname)
        );
        if (found.length > 0) {
          return item;
        }
      });
      setDevice(deviceFound || false);
    }
  }, [searchResults]);

  return (
    <div className={css.root}>
      <Header />
      <div className={css.titleBar}>
        <div className={css.container}>
          <button
            className={css.backButton}
            type="button"
            onClick={() => navigate(-1)}
          >
            <IconBack />
          </button>
          {!error && device?.product && (
            <h1 className={css.productName}>{device.product.name}</h1>
          )}
        </div>
      </div>

      <div className={css.productWrapper}>
        {error || device === false ? (
          <div className={css.product}>
            <div className={css.errorWrapper}>
              <img src={errorImg} />
              <h1>
                <FormattedMessage id="Device.error" />
              </h1>
            </div>
          </div>
        ) : device ? (
          <div className={css.product}>
            <div className={css.productImage}>
              <img
                src={`https://static.ui.com/fingerprint/ui/icons/${device.icon.id}_257x257.png`}
                height="256"
                width="256"
                alt={device.product.name}
              />
            </div>
            <div className={css.productInfo}>
              <table>
                <tbody>
                  <tr>
                    <td>
                      <FormattedMessage id="Device.productLine" />
                    </td>
                    <td>{device.line.name}</td>
                  </tr>
                  <tr>
                    <td>
                      <FormattedMessage id="Device.id" />
                    </td>
                    <td>unifi-network</td>
                  </tr>
                  <tr>
                    <td>
                      <FormattedMessage id="Device.productName" />
                    </td>
                    <td>{device.product.name}</td>
                  </tr>
                  <tr>
                    <td>
                      <FormattedMessage id="Device.shortname" />
                    </td>
                    <td>{device.shortnames.join(", ")}</td>
                  </tr>
                  <tr>
                    <td>
                      <FormattedMessage id="Device.maxPower" />
                    </td>
                    <td>25 W</td>
                  </tr>
                  <tr>
                    <td>
                      <FormattedMessage id="Device.speed" />
                    </td>
                    <td>2400 Mbps</td>
                  </tr>
                  <tr>
                    <td>
                      <FormattedMessage id="Device.numberOfPorts" />
                    </td>
                    <td>5</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        ) : (
          <img src={spinner} alt="Loading…" height="64" width="64" />
        )}
      </div>
    </div>
  );
};

export default Device;
